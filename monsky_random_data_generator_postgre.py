#------------------------------------------------------------------------------
#random IOT data generator for a mongo database
#------------------------------------------------------------------------------
import psycopg2  #sudo apt-get install python3-psycopg2
import random
import datetime
import time
from datetime import datetime, timedelta
import pytz  # Import the pytz library for timezone support
#------------------------------------------------------------------------------
from datetime import datetime, timedelta
#------------------------------------------------------------------------------
#global variables in python
random_data_point_to_generate = 100 * 1000

# PostgreSQL connection information
postgres_host     = "127.0.0.1"   
postgres_port     = 5432          
postgres_db_name  = "monsky" 
postgres_user     = "monsky" 
postgres_password = "guinness"

# Table name to store IoT data
table_name = "iot_data"
#------------------------------------------------------------------------------
def generate_random_iot_data():
    current_datetime = datetime.utcnow()
    # Use pytz to set the desired timezone (e.g., UTC)
    utc = pytz.timezone('UTC')
    current_datetime = utc.localize(current_datetime)
    
    # Format the timestamp as an ISO 8601 string
    timestamp_string = current_datetime.isoformat()
    
    sensor_id = random.randint(1, 10)  # Assuming you have 10 sensors
    value = round(random.uniform(0, 100), 2)  # Random float between 0 and 100
    
    return {
        "timestamp": timestamp_string,  # Use the timestamp as an ISO 8601 string
        "sensor_id": sensor_id,
        "value": value
    }
#------------------------------------------------------------------------------
# Connect to MongoDB
def connect_with_database():
    try:
        conn = psycopg2.connect(
            host=postgres_host,
            port=postgres_port,
            dbname=postgres_db_name,
            user=postgres_user,
            password=postgres_password
        )
        return conn
    except psycopg2.Error as e:
        print("Error connecting to PostgreSQL:", e)
        return None
#------------------------------------------------------------------------------
# Disconnect from PostgreSQL database
def disconnect_from_database(conn):
    conn.close()
  
#------------------------------------------------------------------------------
# Insert random IoT data into MongoDB
def insert_iot_data_into_database(conn, cursor):
    global random_data_point_to_generate
    for _ in range(random_data_point_to_generate):  # Change the number to the desired amount of data points
        iot_data = generate_random_iot_data()
        try:
            cursor.execute(
                f"INSERT INTO {table_name} (timestamp, sensor_id, value) VALUES (%s, %s, %s)",
                (iot_data['timestamp'], iot_data['sensor_id'], iot_data['value'])
            )
            conn.commit()
        except psycopg2.Error as e:
            print("Error inserting data into PostgreSQL:", e)
            conn.rollback()
        
        # Random wait
        time.sleep(1)
#------------------------------------------------------------------------------
def main():
    print("IoT data generation and storage starts")
    conn = connect_with_database()
    if conn is not None:
        cursor = conn.cursor()
        insert_iot_data_into_database(conn, cursor)
        disconnect_from_database(conn)
        print("IoT data generation and storage ends")
    else:
        print("Exiting due to database connection error")
#------------------------------------------------------------------------------           
if __name__ == "__main__":
    main()    
#------------------------------------------------------------------------------
