#------------------------------------------------------------------------------
#random IOT data generator for a mongo database
#------------------------------------------------------------------------------
import pymongo
import random
import datetime
import time
#------------------------------------------------------------------------------
from datetime import datetime, timedelta
#------------------------------------------------------------------------------
#global variables in python
random_data_point_to_generate = 100 * 1000

mongo_client     = "not_defined"
mongo_db         = "not_defined"
mongo_collection = "not_defined"
#------------------------------------------------------------------------------
# MongoDB connection information
mongo_host            = "localhost"  # Replace with your MongoDB server's host
mongo_port            = 27017  # Replace with your MongoDB server's port
mongo_db_name         = "monsky"  # Replace with your database name
mongo_collection_name = "iot_data"  # Replace with your collection name
#------------------------------------------------------------------------------
# Function to generate random IoT data
def generate_random_iot_data():
    
    current_datetime = datetime.utcnow()
    timestamp_milliseconds = int(time.mktime(current_datetime.timetuple()) * 1000)
    sensor_id = random.randint(1, 10)  # Assuming you have 10 sensors
    value = round(random.uniform(0, 100), 2)  # Random float between 0 and 10
    return {"_id": timestamp_milliseconds
            , "sensor_id": sensor_id
            , "value": value
           }
#------------------------------------------------------------------------------
# Connect to MongoDB
def connect_with_database():
    global mongo_client,mongo_db, mongo_collection
    mongo_client     = pymongo.MongoClient(host=mongo_host, port=mongo_port)    
    mongo_db         = mongo_client[mongo_db_name]
    mongo_collection = mongo_db[mongo_collection_name]
#------------------------------------------------------------------------------
def disconnect_from_database():
# Close the MongoDB connection
    mongo_client.close()    
#------------------------------------------------------------------------------
# Insert random IoT data into MongoDB
def insert_iot_data_into_database():
    global random_data_point_to_generate
    for _ in range(random_data_point_to_generate):  # Change the number to the desired amount of data points
        iot_data = generate_random_iot_data()    
        mongo_collection.insert_one(iot_data)
        
        #random wait    
        time.sleep(1)
#------------------------------------------------------------------------------   
def main():
    print("IOT random data generation starts")
    connect_with_database()
    insert_iot_data_into_database()
    disconnect_from_database()
    print("IOT random data generation ends")
#------------------------------------------------------------------------------   
if __name__ == "__main__":
    main()    
#------------------------------------------------------------------------------
